package id.fko.fruitdesc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import id.fko.fruitdesc.adapter.CardViewFruitAdapter;
import id.fko.fruitdesc.data.FruitData;
import id.fko.fruitdesc.model.Fruit;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvFruit;
    private ArrayList<Fruit> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar =findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        rvFruit = findViewById(R.id.rv_fruit);
        rvFruit.setHasFixedSize(true);

        list.addAll(FruitData.getListData());
        showRecyclerCardView();
    }

    private void showRecyclerCardView(){
        rvFruit.setLayoutManager(new LinearLayoutManager(this));
        CardViewFruitAdapter cardViewFruitAdapter = new CardViewFruitAdapter(list);
        rvFruit.setAdapter(cardViewFruitAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_action, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.profile:
                Toast.makeText(this, "Test", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
