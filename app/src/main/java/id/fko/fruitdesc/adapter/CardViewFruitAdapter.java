package id.fko.fruitdesc.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import id.fko.fruitdesc.DetailActivity;
import id.fko.fruitdesc.R;
import id.fko.fruitdesc.model.Fruit;

public class CardViewFruitAdapter extends RecyclerView.Adapter<CardViewFruitAdapter.CardViewFruitHolder>{
    private ArrayList<Fruit> listFruit;

    public CardViewFruitAdapter(ArrayList<Fruit> list) {
        this.listFruit = list;
    }

    @NonNull
    @Override
    public CardViewFruitHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cardview_fruit, viewGroup, false);
        return new CardViewFruitHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardViewFruitHolder cardViewFruitHolder, int position) {
        final Fruit fruit= listFruit.get(position);

        Glide.with(cardViewFruitHolder.itemView.getContext())
                .load(fruit.getPhoto())
                .apply(new RequestOptions().override(350, 550))
                .into(cardViewFruitHolder.imgPhoto);

        cardViewFruitHolder.tv_name_fruit.setText(fruit.getName());
        cardViewFruitHolder.tv_item_scientific_name.setText(fruit.getScientific_name());
        cardViewFruitHolder.tv_item_rank.setText(fruit.getRank());

        cardViewFruitHolder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fruitIntent = new Intent(v.getContext(), DetailActivity.class);
                fruitIntent.putExtra("EXTRA_NAME", fruit.getName());
                fruitIntent.putExtra("EXTRA_DETAIL", fruit.getDetail());
                v.getContext().startActivity(fruitIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFruit.size();
    }

    public class CardViewFruitHolder extends RecyclerView.ViewHolder {
        public ImageView imgPhoto;
        public TextView tv_name_fruit, tv_item_scientific_name, tv_item_rank;

        public Button btnDetail;

        public CardViewFruitHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tv_name_fruit = itemView.findViewById(R.id.tv_item_name);
            tv_item_scientific_name = itemView.findViewById(R.id.tv_item_scientific_name);
            tv_item_rank = itemView.findViewById(R.id.tv_item_rank);
            btnDetail = itemView.findViewById(R.id.btn_detail);
        }
    }
}
