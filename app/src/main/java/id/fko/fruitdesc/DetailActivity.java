package id.fko.fruitdesc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar =findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        TextView tv_name= findViewById(R.id.tv_name_detail);
        TextView tv_detail= findViewById(R.id.tv_detail);

        String name= getIntent().getStringExtra("EXTRA_NAME");
        String detail= getIntent().getStringExtra("EXTRA_DETAIL");


        tv_name.setText(name);
        tv_detail.setText(detail);
    }
}
